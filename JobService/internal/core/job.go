package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type Job struct {
	ID             primitive.ObjectID `bson:"_id,omitempty"`
	Experience     uint               `bson:"experience,omitempty"`
	Skills         []string           `bson:"skills,omitempty"`
	JobDescription string             `bson:"jobDescription,omitempty"`
	Salary         uint               `bson:"salary,omitempty"`
}
